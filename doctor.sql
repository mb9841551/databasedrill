CREATE DATABASE IF NOT EXISTS DOCTOR;
USE DOCTOR;
CREATE TABLE IF NOT EXISTS DOCTOR (
    DOCTOR_ID INT PRIMARY KEY,
    DOCTOR_NAME VARCHAR(100) NOT NULL,
    SECRETARY VARCHAR(100)
);
CREATE TABLE IF NOT EXISTS PRESCRIPTION (
    PRESCRIPTION_ID INT PRIMARY KEY,
    DRUG VARCHAR(100),
    DATE DATE,
    DOSAGE VARCHAR(255)
);
CREATE TABLE IF NOT EXISTS PATIENT (
    PATIENT_ID INT PRIMARY KEY,
    PATIENT_NAME VARCHAR(100) NOT NULL,
    PATIENT_DOB DATE,
    PATIENT_ADDRESS VARCHAR(255),
    DOCTOR_ID INT,
    PRESCRIPTION_ID INT,
    FOREIGN KEY (DOCTOR_ID) REFERENCES DOCTOR(DOCTOR_ID),
    FOREIGN KEY (PRESCRIPTION_ID) REFERENCES PRESCRIPTION(PRESCRIPTION_ID)
);