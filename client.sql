CREATE TABLE Client (
    Client_Id INT PRIMARY KEY,
    Name VARCHAR(100) NOT NULL,
    Location VARCHAR(255)
);
CREATE TABLE Manager (
    Manager_Id INT PRIMARY KEY,
    Manager_Name VARCHAR(100) NOT NULL,
    Manager_Location VARCHAR(255),
    Client_Id INT NOT NULL,
    FOREIGN KEY (Client_Id) REFERENCES Client(Client_Id)
);
CREATE TABLE Contract (
    Contract_Id INT PRIMARY KEY,
    ESTIMATED_COST INT,
    COMPLETION_DATE DATE,
    Manager_Id INT,
    FOREIGN KEY (Manager_Id) REFERENCES Manager(Manager_Id)
);
CREATE TABLE Staff (
    Staff_Id INT PRIMARY KEY,
    Staff_Name VARCHAR(100) NOT NULL,
    Staff_Location VARCHAR(255),
    Client_Id INT,
    FOREIGN KEY (Client_Id) REFERENCES Client(Client_Id)
);